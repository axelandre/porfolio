const config = require('./.contentful.json');
require('@nuxtjs/dotenv');
const contentful = require('contentful');
let client = contentful.createClient({
	space: config.CTF_SPACE_ID,
	accessToken: config.CTF_CDA_ACCESS_TOKEN
});
const axios = require('axios');
module.exports = {
	/*
   ** Headers of the page
   */
	head: {
		titleTemplate: '%s Axel André • Développeur Web Front-end / Back-end et Webdesigner',
		meta: [
			{
				charset: 'utf-8'
			},
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1'
			},
			{
				hid: 'description',
				name: 'description',
				content:
					"Axel André, passionné du monde digitale réalisant du développement web, aussi bien Back-end que Front-end ainsi que du Webdesign. Opérant dans la région de Lille. Actuellement en recherche d'alternance"
			},
			{
				name: 'language',
				content: 'fr-FR'
			}
		],
		link: [
			{
				rel: 'icon',
				type: 'image/x-icon',
				href: '/favicon.ico'
			},
			{
				rel: 'canonical',
				href: 'https://axel-andre.fr'
			},
			{
				rel: 'stylesheet',
				href: '/normalize.css'
			}
		],
		script: [
			{
				src: 'https://www.googletagmanager.com/gtag/js?id=UA-127920147-1',
				defer: true
			},
			{
				src: '/googleAnalytics.js'
			}
		]
	},
	/*
   ** Customize the progress bar color
   */
	loading: {
		color: 'red'
	},
	/*
   ** Build configuration
   */
	build: {
		/*
     ** Run ESLint on save
     */
		extend(config, { isDev, isClient }) {
			if (isDev && isClient) {
				config.module.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					loader: 'eslint-loader',
					exclude: /(node_modules)/
				});
			}
		},
		extend(config, ctx) {
			const vueLoader = config.module.rules.find((rule) => rule.loader === `vue-loader`);
			vueLoader.options.preserveWhitespace = true;
		}
	},
	generate: {
		routes: async function() {
			try {
				const response = await client.getEntries({
					content_type: 'projects',
					order: '-sys.createdAt'
				});
				return response.items.map((e) => '/projets/' + e.fields.slug);
			} catch (e) {
				return console.log(e);
			}
		}
	},
	// Modules
	modules: [ '@nuxtjs/pwa', '@nuxtjs/dotenv', '@nuxtjs/sitemap' ],
	// Plugins
	plugins: [ '~plugins/contentful' ],

	// PWA Configuration
	manifest: {
		name: 'Porfolio de Axel André',
		description: 'Développeur Web Front-end / Back-end et Webdesigner',
		short_name: 'Porfolio de A.A',
		lang: 'fr',
		theme_color: '#D0021B',
		background_color: 'black',
		display: 'standalone',
		ogType: 'website',
		ogHost: 'https://axel-andre.fr',
		ogImage: '/favicon.ico'
	},
	// SITEMAP
	sitemap: {
		path: '/sitemap.xml',
		hostname: 'https://axel-andre.fr',
		cacheTime: 1000 * 60 * 15,
		gzip: true,
		generate: true, // Enable me when using nuxt generate
		exclude: [ '/secret', '/admin/**' ],
		routes: async function() {
			try {
				const response = await client.getEntries({
					content_type: 'projects',
					order: '-sys.createdAt'
				});
				return response.items.map((e) => '/projets/' + e.fields.slug);
			} catch (e) {
				return console.log(e);
			}
		}
	},

	// COMPORTEMENT DU ROUTER
	router: {
		scrollBehavior: function(to, from, savedPosition) {
			return {
				x: 0,
				y: 0
			};
		}
	}
};
